package planning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class OutingController {

	Logger log = LoggerFactory.getLogger(OutingController.class);
	@Value("${STAFF_DNS:empty}")
	private String configMapStaffDns;
	@Value("${STAFF_PORT:empty}")
	private String configMapStaffPort;

	public OutingController() { }
	
	public String ping() {
		return "frontend";
	}

/**
 * Reach out to the back-end micro-service for information about system health.
 * @return String health information
 */
	public String getHealth() {
		this.configMapStaffDns = System.getenv("STAFF_DNS");
		log.info("ConfigMap Dns: " + this.configMapStaffDns);
		this.configMapStaffPort = System.getenv("STAFF_PORT");
		log.info("ConfigMap Port: " + this.configMapStaffPort);
		String resourceUrl = "http://" + configMapStaffDns + ":" + configMapStaffPort + "/health";

		log.info("frontend microservice is calling health url: " + resourceUrl);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
		return response.getBody();
	}

}