package planning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.logging.Logger;

@SpringBootApplication
public class PayrollApplication {
	
	private static final Logger logger = Logger.getLogger(PayrollApplication.class.getName());

	  /** Expects a new or existing Stackdriver log name as the first argument.*/
	  public static void main(String... args) throws Exception {
		    logger.info("Logging INFO from frontend with java.util.logging");
		    logger.severe("Logging SEVERE from frontend with java.util.logging");

	    
		SpringApplication.run(PayrollApplication.class, args);
	  }

}
